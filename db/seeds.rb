folder = "avi_images"
Location.delete_all
Photo.delete_all
Dir.entries(folder).each do |file_name|
	next if File.directory? file_name 
	Photo.create(attachment: File.new("#{folder}/#{file_name}"), flight_summary: { operator_name: "Orville Wright", aircraft_type: "Boing 747", tail_number: "NCC1701", payload: "Spice", notes: "Fragile Cargo." })
end
