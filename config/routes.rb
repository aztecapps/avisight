Rails.application.routes.draw do

  get "/photos/get_data", to: "photos#get_data", defaults: { format: 'json' }

  resources :photos, except: [:edit, :update]
  resources :locations, except: [:new, :create]

  root to: "locations#index"

end
