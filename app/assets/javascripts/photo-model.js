(function ($) {

  function RemoteModel() {

    var PAGESIZE = 50;
    var data = {length: 0};
    var h_request = null;
    var req = null;
    
    var onDataLoading = new Slick.Event();
    var onDataLoaded = new Slick.Event();

    function init() {
    }

    function isDataLoaded(from, to) {
      for (var i = from; i <= to; i++) {
        if (data[i] == undefined || data[i] == null) {
          return false;
        }
      }
      return true;
    }


    function clear() {
      for (var key in data) {
        delete data[key];
      }
      data.length = 0;
    }


    function ensureData(from, to) {
      if(req) {
        req.abort();
        for(var i = req.fromPage; i <= req.toPage; i++)
          data[i * PAGESIZE] = undefined;
      }

      if(from < 0) {
        from = 0;
      }

      var fromPage = Math.floor(from / PAGESIZE);
      var toPage = Math.floor(to / PAGESIZE);

      while (data[fromPage * PAGESIZE] !== undefined && fromPage < toPage)
        fromPage++;

      while (data[toPage * PAGESIZE] !== undefined && fromPage < toPage)
        toPage--;

      if (fromPage > toPage || ((fromPage == toPage) && data[fromPage * PAGESIZE] !== undefined)) {
        onDataLoaded.notify({from: from, to: to});
        return;
      }

      var url = "/photos?start=" + (fromPage * PAGESIZE) + "&limit=" + (((toPage - fromPage) * PAGESIZE) + PAGESIZE);

      if(h_request != null) {
        clearTimeout(h_request);
      }

      h_request = setTimeout(function () {
        for (var i = fromPage; i <= toPage; i++)
          data[i * PAGESIZE] = null;
        
        req = $.getJSON(url, onSuccess).fail(function() { alert("error loading pages"); });
        req.fromPage = fromPage;
        req.toPage = toPage;
      }, 50);
    }

    function onSuccess(resp) {
      var from = parseInt(resp.request.start), to = from + resp.results.length;
      data.length = resp.results.length;
      for (var i = 0; i < resp.results.length; i++) {
        data[from + i] = resp.results[i];
        data[from + i].index = from + i;
      }
      onDataLoaded.notify({from: from, to: to});
    }

    function reloadData(from, to) {
      for (var i = from; i <= to; i++)
        delete data[i];
      ensureData(from, to);
    }

    init();

    return {
      "data": data,
      "clear": clear,
      "editable": true,
      "autoEdit": true,
      "isDataLoaded": isDataLoaded,
      "ensureData": ensureData,
      "reloadData": reloadData,
      "onDataLoading": onDataLoading,
      "onDataLoaded": onDataLoaded
    };
  }

  $.extend(true, window, { Slick: { Data: { RemoteModel: RemoteModel }}});
})(jQuery);