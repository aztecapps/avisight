var grid;
var loader = new Slick.Data.RemoteModel();
var columns = [
  {id: "id", name: "ID", field: "id"},
  {id: "url", name: "URL", field: "url"},
  {id: "make", name: "Make", field: "make", editor: Slick.Editors.Text},
  {id: "model", name: "Model", field: "model", editor: Slick.Editors.Text},
  {id: "exposure_time", name: "Exposure Time", field: "exposure_time", editor: Slick.Editors.Text},
  {id: "iso_speed", name: "ISO Speed", field: "iso_speed", editor: Slick.Editors.Text},
  {id: "exposure_bias", name: "Exposure Bias", field: "exposure_bias", editor: Slick.Editors.Text},
  {id: "focal_length", name: "Focal Length", field: "focal_length", editor: Slick.Editors.Text},
  {id: "max_aperture", name: "Max Aperture", field: "max_aperture", editor: Slick.Editors.Text},
  {id: "metering_mode", name: "Metering Mode", field: "metering_mode", editor: Slick.Editors.Text},
  {id: "operator_name", name: "Operator Name", field: "operator_name", editor: Slick.Editors.Text},
  {id: "aircraft_type", name: "Aircraft Type", field: "aircraft_type", editor: Slick.Editors.Text},
  {id: "tail_number", name: "Tail Number", field: "tail_number", editor: Slick.Editors.Text},
  {id: "payload", name: "Payload", field: "payload", editor: Slick.Editors.Text},
  {id: "flight_notes", name: "Flight Notes", field: "flight_notes", editor: Slick.Editors.LongText},
  {id: "notes", name: "Notes", field: "notes", editor: Slick.Editors.LongText}
];
var options = {
    rowHeight: 21,
    editable: false,
    enableAddRow: false,
    enableCellNavigation: false,
    enableColumnReorder: false
};
var loadingIndicator = null;
$(function () {
  grid = new Slick.Grid("#myGrid", loader.data, columns, options);
  grid.onViewportChanged.subscribe(function (e, args) {
    var vp = grid.getViewport();
    loader.ensureData(vp.top, vp.bottom);
  });
  loader.onDataLoading.subscribe(function () {
    if (!loadingIndicator) {
      loadingIndicator = $("<span class='loading-indicator'><label>Buffering...</label></span>").appendTo(document.body);
      var $g = $("#myGrid");
      loadingIndicator
        .css("position", "absolute")
        .css("top", $g.position().top + $g.height() / 2 - loadingIndicator.height() / 2)
        .css("left", $g.position().left + $g.width() / 2 - loadingIndicator.width() / 2);
    }
    loadingIndicator.show();
  });
  loader.onDataLoaded.subscribe(function (e, args) {
    for (var i = args.from; i <= args.to; i++) {
      grid.invalidateRow(i);
    }
    grid.updateRowCount();
    grid.render();
    if(loadingIndicator) loadingIndicator.fadeOut();
  });
  grid.onViewportChanged.notify();
});
