class GeoLocation
  include Mongoid::Document

  field :loc, type: Hash
  field :lat, type: Float
  field :lng, type: Float

  before_save :set_geolocation

  def set_geolocation
    self.loc = { type: "Point", coordinates: [self.lng, self.lat] }
  end

  def latlng
    coords = self.loc[:coordinates]
    Geokit::LatLng.new(coords[1], coords[0])
  end

  def distance_to(poi)
    self.latlng.distance_to(poi.latlng)
  end
end
