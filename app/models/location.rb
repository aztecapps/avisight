class Location < GeoLocation

  field :poi, type: Integer

  has_many :photos, inverse_of: :location

  def name
    "POI #{self.poi}"
  end

end
