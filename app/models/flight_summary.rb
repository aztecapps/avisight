class FlightSummary
  include Mongoid::Document

  embedded_in :photo, inverse_of: :flight_summary

  field :operator_name, type: String
  field :aircraft_type, type: String
  field :tail_number, type: String
  field :payload, type: String
  field :notes, type: String
end
