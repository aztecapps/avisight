json.id photo.id.to_s
json.url photo.attachment.url(:original)
json.make photo.metadata[:make]
json.model photo.metadata[:model]
json.exposure_time photo.metadata[:exposure_time]
json.iso_speed photo.metadata[:iso_speed_ratings]
json.exposure_bias photo.metadata[:exposure_bias_value]
json.focal_length photo.metadata[:focal_length]
json.max_aperture photo.metadata[:max_aperture_value]
json.metering_mode photo.metadata[:metering_mode]
json.operator_name photo.flight_summary.operator_name
json.aircraft_type photo.flight_summary.aircraft_type
json.tail_number photo.flight_summary.tail_number
json.payload photo.flight_summary.payload
json.flight_notes photo.flight_summary.notes
json.notes photo.notes