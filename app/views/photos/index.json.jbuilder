json.request do |json|
  json.start params[:start]
  json.limit params[:limit]
end

json.results do |json|
  json.array! @photos, partial: 'photos/photo', as: :photo
end