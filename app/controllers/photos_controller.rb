class PhotosController < ApplicationController
  before_action :set_photo, only: [:show, :edit, :update, :destroy]

  def index
    @photos = Photo.all
  end

  def show
  end

  def new
    @photo = Photo.new
    @photo.flight_summary.build
  end

  def edit
  end

  def create
    @flight_summary = FlightSummary.new(flight_params)
    respond_to do |format|
      if @flight_summary.save
        format.html { redirect_to locations_url, notice: 'Photos successfully uploaded.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @flight_summary.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, photo: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photos_url, notice: 'Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def flight_params
      params.fetch(:photo, {}).permit(:operator_name, :aircraft_type, :tail_number, :payload, :notes, photos_attributes: [:attachment])
    end
end
